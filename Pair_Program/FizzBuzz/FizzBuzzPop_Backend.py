#the FizzBuzz problem set
# James G and Jinglei

def enter_number ():
    """
    Enter a valid number, which must be a integer

    """
    print('Enter your number')

    x = int(input())

    return x

def play_game():

    n=enter_number()

    if n % (3*5*7) == 0:
        result = 'FizzBuzzPop'

    elif n%(3*5) == 0:
        result = 'FizzBuzz'

    elif n%(3*7) == 0:
        result = 'FizzPop'

    elif n%(5*7) == 0:
        result = 'BuzzPop'

    elif n%3 == 0:
        result = 'Fizz'

    elif n%5 == 0:
        result = 'Buzz'

    elif n%5 == 0:
        result = 'Pop'

    else:
        result = n

    print(result)


if __name__ == '__main__':


    play_game()