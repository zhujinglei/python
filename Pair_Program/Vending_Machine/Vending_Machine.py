import sys

import termios
import tty

class VendingMachine(object):

    def __init__(self,name):

        self.name=name

        self.currency={'onedollar': 0, 'fiftypence': 10, 'quater': 10, 'dime':10, 'nickles':20}

        self.product_stock={'coke':1, 'crisps':1, 'chocolate':1}

        self.balance = 0

        print("Hi, please insert coins (only 10p, 25p, and 5p accepted)", end='\r\n')
        print("The product we have is coke 1 $, chocolate 0.65 $ and crisps 0.5 $", end='\r\n')


    def display_info(self):

        print('This is machine number', self.name, end='\r\n')
        print('Current available changes', self.currency, end='\r\n')
        print('Current stock is', self.product_stock, end='\r\n')



    def insert_coin(self, coin):
        coin_list = [0.1,0.25,0.5,1, 0.05]

        print("You current amount of money is", self.balance, 'Please Insert', end='\r\n')

        self.coin = coin

        if coin in coin_list:
            self.balance += coin
        else:
            print("Invalid money and insert again", end='\r\n')


        if self.coin == 0.1:
            self.currency['dime'] += 1

        if self.coin == 0.25:
            self.currency['quater'] += 1

        if self.coin == 0.05:
            self.currency['nickles'] += 1

        if self.coin == 0.5:
            self.currency['fiftypence'] += 1

        if self.coin == 1:
            self.currency['onedollar'] += 1

        print("you balance is", self.balance,end='\r\n')



    def select_product(self, product):

        print("Please select your product", end='\r\n')


        product_profile={'coke':1, 'crisps':0.5,'chocolate':0.65}

        self.product = product

        cost = product_profile[self.product]

        print("The product you select is", self.product, end='\r\n')


        if self.product_stock[self.product] > 0:

            if self.balance < cost:

                print("Please insert", (cost-self.balance),"more", end='\r\n')

            else:
                self.balance = self.balance - cost
                self.product_stock[self.product] -=1
                print("Please collect you product", self.product,"/n, you balance is", self.balance, end='\r\n')

            return self.balance
        else:

            print("the product you select is currently out of stock", end='\r\n')
            print("return your money or select other product", end='\r\n')


   # def make_change(self):

   #     if self.balance > 0:

   #         self.coin_return = self.balance

#            self.balance = 0

#        else:
 #           self.balance = 0
  #          self.coin_return=0

   #     return self.coin_return



    def return_coins(self):

        left = self.balance

        for x in range(10):
            for y in range(10):
                for z in range(10):
                    if x * 0.05 + y * 0.25 + z * 0.1 == left:
                        self.currency['dime'] -= z
                        self.currency['nickles'] -= x
                        self.currency['quater'] -= y
                        left=0
                        print("you get", x, 'nickles', y,'quater',z,'dime', end='\r\n')

        print("Balance is reduced from", self.balance,'to 0', end='\r\n')
        print('Insert coins and start another purchase', end='\r\n')


if __name__ == '__main__':

    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    tty.setraw(fd)

    try:
        V1=VendingMachine('1')

        print('Please do action press "d" to distplay the current information of the machine', end='\r\n')
        print("Press q to insert 5p, Press w to inster 10 p, Press e to insert 25 p", end='\r\n')
        print('Please press z to buy coke, Press x to buy crisps and Press c to buy chocolate', end='\r\n')
        print("please press g to get change", end='\r\n')

        while True:
            ch=sys.stdin.read(1)
            sys.stdout.write(ch + '\r' + '\n')

            if ch == 'd' or 'D':
                V1.display_info()
            if ch =='q':
                V1.insert_coin(0.05)
            if ch =='w':
                V1.insert_coin(0.1)
            if ch =='e':
                V1.insert_coin(0.25)

            if ch=='z':
                V1.select_product('coke')

            if ch=='x':
                V1.select_product('crisps')

            if ch=='c':
                V1.select_product('chocolate')

            if ch=='g':
                V1.return_coins()


    finally:
        termios.tcsetattr(fd, termios.TCSANOW, old_settings)



  #V1 = VendingMachine('HI')
  #V1.insert_coin(1)
  #V1.select_product('chocolate')
  #V1.return_coins()
  #print(V1.currency)

  #V1.select_product('coke')


#print(V1.currency)
   # V1.insert_coin(0.5)
   # V1.insert_coin(0.5)
   # V1.select_product('cola')



