#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  9 16:46:11 2018

@author: zhuj
"""
##Look at the function: high leve hwo we write the code

###add more function you need;

## function is used to decomposition and abstraction


def is_even(i):
    
    """ Input: i is a positive integer
    return whether it is a even nunber"""

    
    return i%2==0


is_even(2)


## formal parameter gets bound to yhe value of actual parameter when function is
## called nwe scope/frame/ environment created when entre a fucntion
## scope is mappig of names to objects


def f(x):
    """ the sum of x, interger,
    return the sum"""
    
    x=x+1
    print('in f(x):x=', x)
    return x
x=3
f(3)
z=f(x)
print (z) 


## try to make sure you have return something after running


for i in range(20):
    if is_even(i):
        print(i,"even")
    else:
        print (i,"odd")
        
        
        
