#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 11 10:07:35 2018

@author: zhuj
"""

a=[1,2,3,4,5,6,]
sum = 0
for i in a:
    sum +=i
print(sum)

print (len(a))


def quotient_and_remainder (x,y):
    q=x//y
    r=x%y
    return(q,r)
    
(quot, rem) = quotient_and_remainder(4,5)
quot
rem


### manipulating tuples:

def get_data(aTuple):
    nums=()
    words =()
    
    for t in aTuple:
        nums = nums + (t[0],0)
        if t[1] not in words:
            words = words + (t[1],)
    
    min_n=min(nums)
    max_n=max(nums)
    
    unique_words=len(words)
    return (min_n, max_n, unique_words)
    return nums
    return words
    print (nums)
    print (words)

aTuple=((1,"a"),(2,"b"),(3,"c"),(6,"o"),(10,"b"))

get_data(aTuple)

L = [1,2,3,4,5]

total = 0
for i in range(len(L)):
       total +=L[i]
print (total)


total = 0 
for i in L:
    total += i 
print (total)


## operations on LIST -ADD: the dot is kind of operation:

L = [2,1,3]
L.append(5) 
L.extend([0,6])

## how to convert strings to list and vice versa
## doing operating on the list; consider how many side effects;

L.sort()
L.reverse()
sorted(L)

# cloning a list: 
copy = L[:]

# if you dont want to have the sideeffets you have to clone the list 
# even you have nested list you still have the side effects: 


## recrusion: the idea is to take the same idea to solve the
## i want to solve a problem in a simpler version: call itself 



## loops construct 


def mult_iter(a,b):
    result = 0
    while b>0:
        result += a
        b -=1
    return result

### a simpler = version: of multiplication
    
def mult(a,b):
    if b ==1:
        return a
    else:
        return a+ mult(a, b-1)



def factorial(n):
   if n ==1:
       return 1
   else:
       return n*factorial(n-1)
   


### compare the recursion and iteration:
       
def factorial_iter(n):
    prod = 1 
    for i in range (1, n+1):
        prod *=1
    return prod 



### the recursive
    
def printMove(fr, to):
    print('move from' + str(fr) + 'to' +str(to))
    
def Towers(n,fr,to,spare):
    if n==1:
       printMove(fr, to)
    else:
       Towers(n-1,fr,spare,to)
       Towers(1,fr,to,spare)
       Towers(n-1,spare,to,fr)



printMove (fr,to)
Towers(3,fr,to,spare)


### have multiple base:


def fib(x):
    if x==0 or x==1:
        return 1
    else:
        return fib(x-1)+fib(x-2)
    
    
## recursion on non-numerical data:
        
def toChars(s):
    s=s.lower()
    ans=''
    for c in s:
        if  c in 'abcdefghijklmnopqrstuvwxyz':
            ans = ans + c
            
    return ans

def isPal(s):
    if len(s) <=1:
        return True
    else:
        return s[0]==s[-1] and isPal(s[1:-1])

    return isPal(toChars(s))

### dictionary provides a better and cleaner way
    ### nice to index item of interest directly (not always int)
    ### nice to use one data structure, no separaet lists
    
    
my_dict = {}
grades = {'Ana':'B', 'Bob':'a'}

grades['Alice']='A'

grades

## list is ordered sequences of elements; look up emelments by an integer index
## indicate have an order
## index is an integer; 

## dict: matches "keys" to "values"
## look up one item by another item; no order is guranteerreed; key can be any
## immutable type;


def lyrics_to_frequencies(lyrics):
   myDict = {}
   for word in lyrics:
       if word in myDict:
           myDict[word] +=1
       
       else:
           myDict[word] = 1

   return myDict

### using the dictionary


def most_common_words(freqs):
    values = freqs.values()
    best = max(values)
    words = []
    
    for k in freqs:
        if freqs[k] == best:
            words.append(k)
    return (words, best )            


















