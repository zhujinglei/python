
from math import gcd

class Rational (object):
    def __init__(self, numerator, denominator):

            if denominator < 0:
               numerator = numerator * (-1)
               denominator = denominator * (-1)

            g = gcd(numerator, denominator)
            self.numerator = numerator//g
            self.denominator = denominator//g

            if denominator == 0:
                print("Denominator can not be 0")


    def __str__(self):
        if self.denominator == 1:
            return self.numerator
        else:
            return "{0}/{1}".format(self.numerator, self.denominator)

    def __add(self, other):
        n = self.numerator * other.denominator + self.denominator * other.numerator
        d = self.denominator * other.denominator
        return Rational(n, d)




