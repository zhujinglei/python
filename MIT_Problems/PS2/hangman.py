# Problem Set 2, hangman.py
# Name: Jinglei Zhu
# Collaborators:
# Time spent:

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random
import string

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)
    
    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code

# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()


def is_word_guessed(secret_word, letters_guessed):

    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''
# Convert strings and append them into a list
    element_list = []
    for i in secret_word:
            element_list.append(i)

    return set(element_list) <= set(letters_guessed)
# Identify whether the secret words is the subset of the letters_guessed, if so, return true.

def get_guessed_word (secret_word, letters_guessed ):

    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
# Create an empty string to store and show the guessed word"
    guessedletter = ""
    for i in secret_word:
        if i in letters_guessed:
            guessedletter += i
# if the character in letters_guessed then record the letter
        else:
            guessedletter += '_'
# if not, fill in ' _ '
    return guessedletter


def get_available_letters(letters_guessed):

    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''

# all the words in the file are lower-case, thus create a list which contains all the character
    available_words = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']

    for i in letters_guessed:
        available_words.remove(i)

# if the character is already guessed, then removed form the available list
    return available_words

def hangman1(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Remember to make
      sure that the user puts in a letter!
    
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
    
    Follows the other limitations detailed in the problem write-up.
    '''


# first define the parameters, include the time of guess, warning messages and the list of letters guessed
    num_guesses = 6
    warnings = 3
    letters_guessed = []
# loading the secret word and provide the hint with the length of the word

    print("I am thinking of a word that is ", len(secret_word), "letters long")

# The first conditions that the number of guess is greater than 0 and the word has not been guessed

    while num_guesses >0 and (not (is_word_guessed(secret_word, letters_guessed))):

        print("You have ", num_guesses,"left")
        guessing = get_guessed_word (secret_word, letters_guessed)
        print ("The word now is: ", guessing)
        allowed_words = get_available_letters(letters_guessed)
        print ('Available words are ', allowed_words)
        print('Now you have', warnings, 'warnings')
        charchosed = input("Please guess a letter ")

# the comments before that is printing the relevant information about the guessing game

# the following condition is to warn the user to choose the letter in the suggestion range, and do not include
# the symbols repeated letters, if the waring equals to 0, the guess will be reduced by one

        while warnings > 0 and (charchosed not in allowed_words):
            warnings -= 1
            print('Now you have', warnings,'warnings')
            charchosed = input(" Oops! That letter is not in my suggestion "
                               "list, please input again it cannot be a symbol or repeated char ")
        letters_guessed.append(charchosed)

        if warnings == 0:
            num_guesses -= 1
# if the character choosed not in the secret word, the number of guesses will be reduced by one
        if charchosed not in secret_word and charchosed in allowed_words:
            num_guesses -= 1
            if charchosed in ['a','e','i','o','u']:
                num_guesses -= 1

        print('\n')

# Calculate the score
        score = num_guesses * len(secret_word)

# Print the guess result
    if is_word_guessed(secret_word, letters_guessed):
        print ("Congratulations, you won!", score)
    else:
        print ("Sorry, you ran out of guesses. The word was", secret_word)

pass
# When you've completed your hangman function, scroll down to the bottom
# of the file and uncomment the first two lines to test

# hint: you might want to pick your own
# secret_word while you're doing your own testing)
# -----------------------------------



def match_with_gaps(my_word, other_word):

    '''
    my_word: string with _ characters, current guess of secret word
    other_word: string, regular English word
    returns: boolean, True if all the actual letters of my_word match the 
        corresponding letters of other_word, or the letter is the special symbol
        _ , and my_word and other_word are of the same length;
        False otherwise: 
    '''
# define the word length should be the same
    if len(my_word) != len(other_word):
        return False

    guessed_letter = set()  # define the guessed

# to prevent the user input the ' _ ' and increasing the length of the word
    for i in my_word:
        if i != '_':
            guessed_letter.add(i)

# compare my word with other words to check whether it is matched the gap
    for j in range(len(my_word)):
        if my_word[j] != other_word[j] and my_word[j] != '_':
            return False
        if my_word[j] == '_' and (other_word[j] in guessed_letter):
            return False
    return True




def show_possible_matches(my_word):

    ''' my_word: string with _ characters, current guess of secret word
    returns: nothing, but should print out every word in wordlist that matches my_word
             Keep in mind that in hangman when a letter is guessed, all the positions
             at which that letter occurs in the secret word are revealed.
             Therefore, the hidden letter(_ ) cannot be one of the letters in the word
             that has already been revealed. '''

# print the possible matching word in the word list, which has the same length of the word and match the gap

    print('Show possible matching word ')
    for word in wordlist:
        if (match_with_gaps(my_word, word)):
            print('Possible matching words are: ', word)


def hangman_with_hints(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses
    
    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Make sure to check that the user guesses a letter
      
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
      
    * If the guess is the symbol *, print out all words in wordlist that
      matches the current guessed word. 
    
    Follows the other limitations detailed in the problem write-up.
    '''
# first define the parameters, include the time of guess, warning messages and the list of letters guessed
    num_guesses = 6
    warnings = 3
    letters_guessed = []

# loading the secret word and provide the hint with the length of the word
    print("I am thinking of a word that is ", len(secret_word), "word")

# The first conditions that the number of guess is greater than 0 and the word has not been guessed
    while num_guesses > 0 and not (is_word_guessed(secret_word, letters_guessed)):

        print("You have ", num_guesses, "left")
        guessing = get_guessed_word(secret_word, letters_guessed)
        print("The word now is: ", guessing)
        allowed_words = get_available_letters(letters_guessed)
        print('Available words are ', allowed_words)
        print('Now you have', warnings, 'warnings')
        charchosed = input("Please guess a letter ")

# provide the function of the hint, when the user type in '*', then the computer provides the possible matching words
        if charchosed == '*':
            show_possible_matches(guessing)
            continue

# the following condition is to warn the user to choose the letter in the suggestion range, and do not include
# the symbols repeated letters, if the waring equals to 0, the guess will be reduced by one

        while warnings > 0 and (charchosed not in allowed_words):
            warnings -= 1
            print('Now you have', warnings, 'warnings')
            charchosed = input("Oops! That letter is not in my suggestion list, please "
                               "input again it cannot be a symbol or repeated char")
        letters_guessed.append(charchosed)


# the following condition is to warn the user to choose the letter in the suggestion range, and do not include
# the symbols repeated letters, if the waring equals to 0, the guess will be reduced by one

        if warnings == 0:
            num_guesses -= 1
# if the character choosed not in the secret word, the number of guesses will be reduced by one, if the word
        #guessed is a e i o u and it is not the in the secret word, it will reduced extra chance
        if charchosed not in secret_word and charchosed in allowed_words:
            num_guesses -= 1
            if charchosed in ['a', 'e', 'i', 'o', 'u']:
                num_guesses -= 1

            print('\n')

            # Calculate the score
            score = num_guesses * len(secret_word)

        # Print the guess result
        if is_word_guessed(secret_word, letters_guessed):
            print("Congratulations, you won!", score)
        else:
            print("Sorry, you ran out of guesses. The word was", secret_word)

        
# When you've completed your hangman_with_hint function, comment the two similar
# lines above that were used to run the hangman function, and then uncomment
# these two lines and run this file to test!
# Hint: You might want to pick your own secret_word while you're testing.



if __name__ == "__main__":
    # pass

    # To test part 2, comment out the pass line above and
    # uncomment the following two lines.

# the first hangman game

    secret_word = choose_word(wordlist)
#    hangman1(secret_word)


# the second hangman game with hint
#    secret_word = choose_word(wordlist)
    hangman_with_hints(secret_word)






