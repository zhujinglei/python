from flask import Flask
app = Flask(__name__)
from flask import request, render_template
import random
import copy
app.debug = True
from movieDB import *
import re


movieListFromFile = loadLocalDB()
movieDB = MovieDB(movieListFromFile)
moveList = movieDB.getMovieList()

COLOR_SET = ['#862d59', '#cc6699', '#e6b3cc', '#b3cccc', '#669999', '#527a7a', '#ff9900', '#ffcc80',' #0073e6', '#66b3ff', '#ffcc66', '#ffccff']

def reformatList(list):
    for item in list:
        item['Actors'] = ", ".join(item['Actors'])
        item['Genre'] = ", ".join(item['Genre'])

@app.route("/")
def hello():
    return "Hello World!"
@app.route('/test1', methods=('GET', 'POST'))
def sendBack():
    if request.method == 'POST':
        if (request.form.get('Tips') == 'Tips'):
            data = movieDB.tips1()
            return render_template('index1.html', data=data['value'], labels=data['keySet'], chartType='pie',
                               color=random.sample(COLOR_SET, len(data['value'])),
                               chartTitle='trail')
        # serach actor
        if(request.form.get('serachOption') == 'Actor'):
            value = request.form.get('text')
            filterList = copy.deepcopy(movieDB.filterGenreActor(moveList, 'Actors', [value]))
            reformatList(filterList)
            return render_template('index1.html', list=filterList, display="block", border='1')

        if (request.form.get('serachOption') == 'Title'):
            value = request.form.get('text')
            filterList = copy.deepcopy(movieDB.filterTitle(moveList, value))
            reformatList(filterList)
            return render_template('index1.html', list=filterList, display="block", border='1')

        if (request.form.get('serachOption') == 'Director'):
            value = request.form.get('text')
            filterList = copy.deepcopy(movieDB.filterDirector(moveList, value))
            reformatList(filterList)
            return render_template('index1.html', list=filterList, display="block", border='1')

        if (request.form.get('serachOption') == 'Genre'):
            value = request.form.get('text')
            filterList = copy.deepcopy(movieDB.filterGenreActor(moveList, 'Genre', [value]))
            reformatList(filterList)
            return render_template('index1.html', list=filterList, display="block", border='1')

        if (request.form.get('serachOption') == 'Rating'):
            value = request.form.get('text')
            num =re.findall("\d+", value)[0]
            if(value.find('>')>=0):
                filterList = copy.deepcopy(movieDB.filterRange(moveList, 'Rating', lowerLimit=float(num)))
            else:
                filterList = copy.deepcopy(movieDB.filterRange(moveList, 'Rating', upperLimit=float(num)))
            reformatList(filterList)
            return render_template('index1.html', list=filterList, display="block", border='1')


        return render_template('index1.html',display='none',border='0')

    # display the web page
    return render_template('index1.html',display='none',border='0')

