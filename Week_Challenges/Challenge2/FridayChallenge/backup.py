from flask import Flask
app = Flask(__name__)
from flask import request, render_template
import random
import copy
app.debug = True
from movieDB import *
import json
import re


movieListFromFile = loadLocalDB()
movieDB = MovieDB(movieListFromFile)
moveList = movieDB.getMovieList()

COLOR_SET = ['#862d59', '#cc6699', '#e6b3cc', '#b3cccc', '#669999', '#527a7a', '#ff9900', '#ffcc80',' #0073e6', '#66b3ff', '#ffcc66', '#ffccff']

def reformatList(list):
    for item in list:
        item['Actors'] = ", ".join(item['Actors'])
        item['Genre'] = ", ".join(item['Genre'])

@app.route("/")
def hello():
    return "Hello World!"
@app.route('/test1', methods=('GET', 'POST'))
def sendBack():
    if request.method == 'POST':
        if (request.form.get('Tips') == 'Tips'):
            data = movieDB.tips1()
            return render_template('index1.html', data=data['value'], labels=data['keySet'], chartType='pie',
                               color=random.sample(COLOR_SET, len(data['value'])),
                               chartTitle='Revenue Chart', desp='The top 10 movies\' revenue are about 15% of total 400 movies. ', last=[{'null':1234}])
        # serachs
        elif len(json.loads(request.form.get('text')))>0:
            data = json.loads(request.form.get('text'))
            print(data)
            list = moveList.copy()
            for serach in data:
                for key in serach:
                    if key == 'Actor':
                        list = movieDB.filterGenreActor(list, 'Actors', [serach[key]])
                    if key == 'Genre':
                        list = movieDB.filterGenreActor(list, 'Genre', [serach[key]])
                    if key == 'Title':
                        list = movieDB.filterTitle(list, serach[key])
                    if key == 'Director':
                        list = movieDB.filterDirector(list, serach[key])
                    if key == 'Rating':
                        if (serach[key].find('>')>=0):
                            list = movieDB.filterRange(list, 'Rating', lowerLimit=float(re.findall('\d+', serach[key])[0]))
                        elif (serach[key].find('<')>=0):
                            list = movieDB.filterRange(list, 'Rating', upperLimit=float(re.findall('\d+', serach[key])[0]))
                        else:
                            list =[]

            list = copy.deepcopy(list)
            if (len(list)==0):
                data = [{'null':1234}]
            reformatList(list)
            return render_template('index1.html', list=list, display="block", border='1', last=data)
        return render_template('index1.html',display='none',border='0', last=[{'null':1234}])

    # display the web page
    return render_template('index1.html',display='none',border='0', last=[{'null':1234}])




