#!/Users/lir/ubuntu/shared/PythonProject/WeekCh4/venv/python
from classDefinition import LoginHelper, User
import json, flask_login
from flask import render_template, Flask, request, redirect, url_for
from flask_login import current_user, login_user, logout_user



# generate a helper object when the server start
loginHelper = LoginHelper()
# store the public key as string for login
pubkey = loginHelper.getPubKey()
# create a login_manager for flask_login
login_manager = flask_login.LoginManager()


# prepare the server
app = Flask(__name__)
app.secret_key = "super secret key"
login_manager.init_app(app)
app.debug = True


@login_manager.user_loader
def load_user(user_id):
    print('Try to get: ', user_id)
    id, username = loginHelper.getUser(user_id)
    if id != None:
        return User(id, username)
    else:
        return None



@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        # get data from the post request
        data = request.get_data()
        code, id = loginHelper.processLoginRegister(data)
        print(loginHelper.RESPONSE_CODE[code])
        # log in successfully
        if code == 201:
            user = User(id, 'a')
            loginHelper.userDict[id] = user
            login_user(user)
        else:
            logout_user()
        return str(id)+'/1'
    return render_template('index.html', pubkey=pubkey)





@app.route('/<userid>/<blogid>', methods=['GET','POST'])
def blog(userid, blogid):
    if request.method == 'GET':
        if current_user.is_authenticated:
            f = open('./static/dd.txt')
            txt = ''
            for line in f:
                txt += line
            # return redirect(render_template('index4.html', data=data2))

            return render_template('index4.html', data=txt, user=current_user)

        else:
            return 'STOP'



if __name__ == '__main__':
     app.run(debug=True)
