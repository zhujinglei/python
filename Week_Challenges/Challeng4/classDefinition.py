from base64 import b64decode
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import json, Crypto, pymysql, time
from passlib.hash import pbkdf2_sha256
import flask_login as fl

from hashlib import md5




class LoginHelper(object):
    RESPONSE_CODE = {
        100: 'empty username or password',
        101: 'no account',
        102: 'wrong password',
        103: 'username is already in use',
        201: 'successful log in ',
        202: 'successful register'
    }
    def __init__(self):
        self.key = RSA.generate(2048)
        self._publicKeyString = self.key.publickey().export_key('PEM').decode("utf-8").replace("\n","",999)
        self.cipher = PKCS1_OAEP.new(self.key, hashAlgo=Crypto.Hash.SHA256)
        self.setupDataBaseConnection()
        self.userDict = {}
        self.getAllUsers()

    def getAllUsers(self):
        sql = 'select id, user_name from user;'
        self.cursor.execute(sql)
        data = self.cursor.fetchall()
        for one in data:
            self.userDict[str(one[0])] = one[1]

    def decodeLoginInfor(self, data):
        # covert bytes array to string with utf8 format and then load to a dict as JSON
        loginInfor = json.loads(data.decode('utf8'))
        # username is stored as it is
        username = loginInfor['username']
        # password is bytes array encoded as 64base, decode before decryption
        encrypPassword = b64decode(loginInfor['password'])
        # get the real password
        password = self.cipher.decrypt(encrypPassword).decode('utf8')
        return [username, password]

    def getPubKey(self):
        return self._publicKeyString

    def setupDataBaseConnection(self):
        self.db = pymysql.connect(host='localhost',
                                  user='root',
                                  db='blog',
                                  charset='utf8mb4',
                                  autocommit=True
                                 )
        self.cursor = self.db.cursor()

    def login(self, username, password):
        # if username or password is empty
        if len(str(username)) == 0 or len(str(password)) == 0:
            # error code 100: empty username or password
            return 100
        # pull infor from db
        print('Execute sql')
        rowCount = self.cursor.execute('select user_password, id from user where user_account=%s',(username))
        print(rowCount)
        # 0 infor is collected, return false
        print('Finish sql')
        if rowCount == 0:
        # error code 101: no account
            return 101, 0
        passwordHash, userid = self.cursor.fetchone()
        print('password hash is: ', passwordHash)
        print('userid is: ', userid)
        if(pbkdf2_sha256.verify(password, passwordHash)):
            # correct username and password
            return 201, userid
        else:
            # error code: wrong password
            return 102, 0

    def register(self,username, password):
        if len(str(username)) == 0 or len(str(password)) == 0:
            # error code 100: empty username or password
            return 100, 0
        rowCount = self.cursor.execute('select user_password from user where user_account=%s', (username))
        # username already used
        if rowCount > 0:
            # error code duplicate username
            return 103, 0
        passwordHash = pbkdf2_sha256.hash(password)
        timeStr = str(int(time.time()))
        sql = "insert into user (user_name, user_password, user_joindate, user_account) VALUES ('{0}', '{1}', {2}, '{3}');".\
            format(username, passwordHash, timeStr, username)
        print(sql)
        self.cursor.execute(sql)
        print(self.cursor.fetchall())
        # self.cursor.execute('select last_insert_id();')
        # userId = self.cursor.fetchone()[0]
        return 202, 0

    def processLoginRegister(self, data):
        # covert bytes array to string with utf8 format and then load to a dict as JSON
        loginInfor = json.loads(data.decode('utf8'))
        username, password = self.decodeLoginInfor(data)
        print('username: ', username, 'password: ', password)
        if (loginInfor['action'] == 'login'):
            print('Process login')
            return self.login(username, password)
        else:
            print('Process register')
            return self.register(username, password)

    def getUser(self, userId):
        if userId in self.userDict:
            return userId, self.userDict[userId]
        else:
            return None, None


class User(fl.UserMixin):

    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.email = name

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

